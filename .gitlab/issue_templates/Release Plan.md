<!-- markdownlint-disable MD041 -->
## Summary

Should be closed when all these tasks are resolved:

| Issue  | Merge request | Name         | Status           |
|--------|---------------|--------------|------------------|
| #issue | !request(s)   | Resolve this | ~"Ready to test" |
|        |               |              |                  |

## Checklists

All tasks are documented in the [release handbook](https://sat-mtl.gitlab.io/valorisation/internal-docs/guides/release/).

### Requirements

- [ ] [Create a milestone `<project> <version>`](https://gitlab.com/sat-mtl/tools/haptic-floor/-/milestones/new)
- [ ] Tag all issues planned for this release
- [ ] [Create an issue with a release overview](https://gitlab.com/sat-mtl/tools/haptic-floor/mmesh/-/issues/new?issue[title]=Test%20and%20release%20candidate%20x.x.x&issuable_template=Release%20Plan)

### Starting

- [ ] [Create the branch `candidate/x.x.x`](https://gitlab.com/sat-mtl/tools/haptic-floor/mmesh/-/branches/new?branch_name=candidate%2Fx.x.x&ref=develop)
- [ ] [Create a merge request from `candidate/x.x.x` to `master`](https://gitlab.com/sat-mtl/tools/haptic-floor/mmesh/-/merge_requests/new?merge_request[target_branch]=master&merge_request[source_branch]=candidate/x.x.x)

### Updating

- [ ] Rebase candidate branch onto `develop`
- [ ] Update all files:
  - [ ] CHANGELOG.md
  - [ ] AUTHORS.md
  - [ ] README.md
- [ ] Update the software version in
  - [ ] package.json
  - [ ] README.md
- [ ] Make announcement in the QA channel with the information:
  - [ ] All added fixes and features
  - [ ] A link to this issue with an updated overview

### Ending

- [ ] Update all files:
  - [ ] CHANGELOG.md
  - [ ] AUTHORS.md
  - [ ] README.md
- [ ] Update the software version in
  - [ ] package.json
  - [ ] README.md
- [ ] Merge the MR from the candidate branch to `master` or `main`
- [ ] Tag the production branch with
  - [ ] the current version
  - [ ] the last CHANGELOG
- [ ]  Create a merge request from `master` to `develop`
  - [ ] Merge back every change
- [ ] Close the milestone and this issue

/milestone %"<milestone>"

/label ~"flow::in progress"
/label ~"type::release candidate"
/label ~"project::haptic-floor" ~"team::ptt"

/weight 2
